package main

import (
	"testing"
)

func TestNewDateRange(t *testing.T) {
	_, err := NewDateRange("2022-06-02", "2022-06-01")
	if err == nil {
		t.Error("Input should have been rejected")
	}

	dates, err := NewDateRange("2022-06-01", "2022-06-20")
	if err != nil {
		t.Error("Input was valid, but has been rejected")
	}
	if len(dates) != 20 {
		t.Error("Range has wrong length of", len(dates), "vs expected", 20)
	}

	_, err = NewDateRange("2026-06-01", "2026-06-20")
	if err == nil {
		t.Error("Input should have been rejected")
	}
	_, err = NewDateRange("2020-06-01", "2026-06-20")
	if err == nil {
		t.Error("Input should have been rejected")
	}
	
	dates, err = NewDateRange("2021-05-22", "2021-06-10")
	if err != nil {
		t.Error("Input was valid, but has been rejected")
	}
	if len(dates) != 20 {
		t.Error("Range has wrong length of", len(dates), "vs expected", 20)
	}
}

func TestRequestImage(t *testing.T)  {
	dates := []string{
		"2019-12-06",
		"2020-12-06",
	}
	correct_urls := []string{
		"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg",
		"https://apod.nasa.gov/apod/image/2012/pillars8_hst_960.jpg",
	}
	urlcount := 0
	check_url := func(url string, err error, status int) {
		if err != nil {
			t.Error("Input was valid, but has been rejected")
		}
		if status != 200 {
			t.Error("Input was valid, but the request failed")
		}
		correct_url := correct_urls[urlcount]
		if url != correct_url {
			t.Error("The returned URL is different than expected")
		}
		urlcount++
	}
	for _, v := range dates {
		check_url(RequestImage(v))
	}
}
