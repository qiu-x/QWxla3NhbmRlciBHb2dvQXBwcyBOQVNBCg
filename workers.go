package main

type WorkerPool struct {
	workerCount int
	TaskChan    chan func()
}

func NewWorkerPool(count int) *WorkerPool {
	return &WorkerPool{
		workerCount: count,
		TaskChan:    make(chan func()),
	}
}

func (wp *WorkerPool) Run() {
	for i := 0; i < wp.workerCount; i++ {
		go func(workerID int) {
			for task := range wp.TaskChan {
				task()
			}
		}(i + 1)
	}
}

func (wp *WorkerPool) AddTask(task func()) {
	wp.TaskChan <- task
}
