package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
)

var (
	PORT         string
	API_KEY      string
	WORKER_COUNT int
)

var UrlWorkers *WorkerPool

func init() {
	var err error
	var set bool
	PORT, set = os.LookupEnv("PORT")
	if !set {
		PORT = "8080"
	}
	API_KEY, set = os.LookupEnv("API_KEY")
	if !set {
		API_KEY = "DEMO_KEY"
	}
	worker_count_str, set := os.LookupEnv("CONCURRENT_REQUESTS")
	if !set {
		WORKER_COUNT = 5
	} else {
		WORKER_COUNT, err = strconv.Atoi(worker_count_str)
	}
	if err != nil {
		log.Fatalln(err)
	} else if WORKER_COUNT <= 0 {
		log.Fatalln("CONCURRENT_REQUESTS must be greater than 0")
	}
}

func CheckMethod(t string, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if t != r.Method {
			http.Error(w, http.StatusText(http.StatusMethodNotAllowed),
				http.StatusMethodNotAllowed)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func main() {
	UrlWorkers = NewWorkerPool(WORKER_COUNT)
	UrlWorkers.Run()
	mux := http.NewServeMux()
	mux.Handle("/pictures", CheckMethod("GET", http.HandlerFunc(UrlsRequest)))

	log.Println("Listening on port: ", PORT)
	if err := http.ListenAndServe(":"+PORT, mux); err != nil {
		log.Fatalf("Server error: %s", err.Error())
	}
}
