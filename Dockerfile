FROM alpine:latest
RUN apk add --no-cache go
COPY . /app
WORKDIR /app
ARG port=8080
ENV PORT $port
ARG api_key=DEMO_KEY
ENV API_KEY $api_key
ARG threads=5
ENV CONCURRENT_REQUESTS $threads
RUN go build

ENTRYPOINT ["/app/gogo_space"]
