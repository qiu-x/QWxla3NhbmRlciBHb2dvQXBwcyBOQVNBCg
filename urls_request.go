package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/validator.v2"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

var NASA_URL = "https://api.nasa.gov/planetary/apod"

func NewDateRange(from, to string) ([]string, error){
	date_layout := "2006-01-02"
	var dates []string
	start, err := time.Parse(date_layout, from)
	end, err := time.Parse(date_layout, to)
	if err != nil {
		return dates, err
	}
	if start.Before(end.AddDate(0, 0, 1)) == false {
		return dates, errors.New("Date range is invalid")
	}
	if start.After(time.Now()) || end.After(time.Now()) {
		return dates, errors.New("Date range is in the future")
	}
	for d := start; d.After(end) == false; d = d.AddDate(0, 0, 1) {
		dates = append(dates, d.Format(date_layout))
	}
	return dates, nil
}

func UrlsRequest(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing request...")

	var image_urls []string
	var req_err error

	// Handle errors and send response
	defer func() {
		var response_json []byte
		var json_err error
		if req_err != nil {
			log.Println("Request Failed!:", req_err.Error())
			response := struct {
				Error string `json:"error"`
			}{req_err.Error()}
			response_json, json_err = json.Marshal(response)
		} else {
			response := struct {
				Urls []string `json:"urls"`
			}{image_urls}
			response_json, json_err = json.Marshal(response)
		}
		if json_err != nil {
			log.Println(json_err.Error())
			return
		}
		w.Write(response_json)
	}()

	from := r.URL.Query().Get("from")
	to := r.URL.Query().Get("to")

	if from == "" || to == "" {
		req_err = errors.New("Invalid arguments")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	dates, err := NewDateRange(from, to)
	if err != nil {
		req_err = err
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	total_tasks := len(dates)
	result_chan := make(chan struct {
		string
		error
		int
	}, total_tasks)

	for i := 0; i < total_tasks; i++ {
		d := dates[i]
		UrlWorkers.AddTask(func() {
			url, err, status := RequestImage(d)
			result_chan <- struct {
				string
				error
				int
			}{url, err, status}
		})
	}

	for i := 0; i < total_tasks; i++ {
		res := <-result_chan
		if res.error != nil {
			req_err = res.error
			w.WriteHeader(res.int)
			return
		}
		image_urls = append(image_urls, res.string)
	}
	w.WriteHeader(http.StatusOK)
	log.Println("Request Completed Sucessfuly!")
}

func RequestImage(date string) (string, error, int) {
	params := "api_key=" + url.QueryEscape(API_KEY) + "&" +
		"date=" + url.QueryEscape(date)
	path := fmt.Sprintf(NASA_URL+"?%s", params)

	resp, err := http.Get(path)
	if err != nil {
		return "", err, resp.StatusCode
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err, resp.StatusCode
	}

	resp_data := struct {
		Url string `json:"url" validate:"nonzero"`
	}{}
	json.Unmarshal(body, &resp_data)
	if err := validator.Validate(resp_data); err != nil {
		return "", errors.New(err.Error() + "; " + resp.Status), resp.StatusCode
	}
	return resp_data.Url, nil, http.StatusOK
}
